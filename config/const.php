<?php

return [
    'web_domain' => env('WEB_DOMAIN'),
    'cms_domain' => env('CMS_DOMAIN'),
    'app_domain' => env('APP_DOMAIN'),
];

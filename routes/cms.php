<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Cms')->group(function () {
    Route::group(['middleware' => ['web']], function () {

        // 管理者ログイン・ログアウト
        Route::namespace('Auth')->group(function () {
            Route::get('login', 'LoginController@showLoginForm')->name('login');
            Route::post('login', 'LoginController@login');
            Route::post('logout', 'LoginController@logout')->name('logout');
        });

        // ログイン認証後
        Route::middleware('auth:cms')->group(function () {

            Route::get('/', 'TemplateController@index');

            // 記事コラム投稿

            // 動画投稿
            Route::resource('videos', 'VideoController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);

            // ユーザ管理
            Route::resource('/touring/user', 'TouringUserController', ['only' => ['index', 'edit', 'update']]);

            // お知らせ
            Route::resource('information', 'InformationController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);

        });
    });
});

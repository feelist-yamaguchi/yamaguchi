<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Api',
    'prefix' => 'user'
], function () {
    Route::get('/', 'TemplateController@index');
    Route::post('login', 'LoginController@login')->name('user.login');
});

Route::group([
    'namespace' => 'Api',
    'prefix' => 'user',
    'middleware' => 'auth:api'
], function () {
    Route::post('logout', 'LoginController@logout');
    Route::post('refresh', 'LoginController@refresh');
    Route::post('me', 'LoginController@me');
});

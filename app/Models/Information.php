<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    use HasFactory;

    /**
     * テーブルの主キー
     *
     * @var string
     */

    // protected $table = 'information';
    protected $primaryKey = 'information_id';
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Touring_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        // token の生成
        $token = auth()->attempt($credentials);
        $token_lifetime = auth()->factory()->getTTL();

        // メールアドレス　パスワードのチェック
        if (! $token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // DBに登録
        $touring_user = Touring_user::where('user_id',auth()->user()->user_id)->first();
        $touring_user->token= $token;
        $touring_user->token_lifetime = $token_lifetime;
        $touring_user->save();

        // token token保持時間 リフレッシュトークン を返す
        return $this->respondWithToken($token, $token_lifetime, $touring_user->refresh_token);
    }

    /**
     * Get the authenticated touring_user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the touring_user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token, $token_lifetime, $refresy_token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $token_linftime, $refresh_token)
    {
        return response()->json([
            'user_id' => auth()->user()->user_id,
            'user_name' => auth()->user()->user_name,
            'email' => auth()->user()->email,
            'token' => $token,
            'token_refresh' => $refresh_token,
            'token_lifetime' => $token_linftime
        ]);
    }
}

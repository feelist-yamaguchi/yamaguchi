<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\Touring_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class TouringUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:cms');
    }

    /**
     * ユーザ一覧
     * 
     * @return view ユーザ一覧画面
     */
    public function index()
    {

        $touring_users = DB::table('touring_users')->orderBy('created_at', 'desc')->paginate(20);

        return view('/cms/user/index', compact('touring_users'));
    }

    /**
     * ユーザステータス変更
     * 
     * @param $id user_id
     * @return view ユーザステータス変更画面
     */
    public function edit($id)
    {
        $touring_user = DB::table('touring_users')->where('user_id', $id)->first();

        // 値が入っている場合datetimeからdateへ型変換
        empty($touring_user->hokkaido_expiration_time) ?: $touring_user->hokkaido_expiration_time = date_format(date_create($touring_user->hokkaido_expiration_time), 'Y-m-d');
        empty($touring_user->tohoku_expiration_time) ?: $touring_user->tohoku_expiration_time = date_format(date_create($touring_user->tohoku_expiration_time), 'Y-m-d');
        empty($touring_user->kanto_expiration_time) ?: $touring_user->kanto_expiration_time = date_format(date_create($touring_user->kanto_expiration_time), 'Y-m-d');
        empty($touring_user->chubu_expiration_time) ?: $touring_user->chubu_expiration_time = date_format(date_create($touring_user->chubu_expiration_time), 'Y-m-d');
        empty($touring_user->kansai_expiration_time) ?: $touring_user->kansai_expiration_time = date_format(date_create($touring_user->kansai_expiration_time), 'Y-m-d');
        empty($touring_user->chugokushikoku_expiration_time) ?: $touring_user->chugokushikoku_expiration_time = date_format(date_create($touring_user->chugokushikoku_expiration_time), 'Y-m-d');
        empty($touring_user->kyushu_expiration_time) ?: $touring_user->kyushu_expiration_time = date_format(date_create($touring_user->kyushu_expiration_time), 'Y-m-d');

        // 課金有無をチェック
        if (
            empty($touring_user->hokkaido_expiration_time) && empty($touring_user->tohoku_expiration_time)
            && empty($touring_user->kanto_expiration_time) && empty($touring_user->chubu_expiration_time)
            && empty($touring_user->kansai_expiration_time) && empty($touring_user->chugokushikoku_expiration_time)
            && empty($touring_user->kyushu_expiration_time)
        ) {
            $billing = false;
        } else {
            $billing = true;
        }

        return view('/cms/user/update', compact('touring_user', 'billing'));
    }

    /**
     * ユーザステータス変更DB登録
     * 
     * @param Request $request フォーム内容
     * @param $id user_id
     * @return redirect ユーザ一覧画面
     */
    public function update(Request $request, $id)
    {
        $touring_user = Touring_user::where('user_id', $id)->first();
        $touring_user->hokkaido_expiration_time = $request->hokkaido_expiration_time;
        $touring_user->tohoku_expiration_time = $request->tohoku_expiration_time;
        $touring_user->kanto_expiration_time = $request->kanto_expiration_time;
        $touring_user->chubu_expiration_time = $request->chubu_expiration_time;
        $touring_user->kansai_expiration_time = $request->kansai_expiration_time;
        $touring_user->chugokushikoku_expiration_time = $request->chugokushikoku_expiration_time;
        $touring_user->kyushu_expiration_time = $request->kyushu_expiration_time;
        $touring_user->save();

        return redirect("/touring/user")->with('message',  'ユーザID'.$id.'のステータスを変更しました。');
    }
}

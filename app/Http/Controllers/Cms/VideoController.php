<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:cms');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // 動画一覧画面表示
        $videos = Video::orderBy('posted_date', 'desc')->paginate(10);
        return view('cms.videos.index', [
            'videos' => $videos,
            'user' => $request->user(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // 動画登録画面表示
        return view('cms.videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 動画登録
        $video = new Video;
        $video->video_title = $request->video_title;
        $video->posted_date = $request->posted_date;
        $video->video_url = $request->video_url;
        $video->video_category_name = $request->category_name;
        $video->video_category_color = $request->category_color;
        if (strpos($request->video_url,'?v=')) {
            $arr = explode("?v=", $request->video_url);
            $video->video_url_id = $arr[1];
            $video->image_url = "https://img.youtube.com/vi/".$arr[1]."/maxresdefault.jpg";
        }
        $video->created_by = $request->user()->admin_user_id;
        $video->updated_by = $request->user()->admin_user_id;
        $video->save();

        return redirect()->route('videos.index')->with('message', '動画の登録が完了しました');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // 動画編集画面表示
        $video = Video::find($id);
        $video->posted_date = str_replace(" ", "T", $video->posted_date);
        return view('cms.videos.update', [
            'video' => $video,
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 動画更新
        $video = Video::find($id);
        $video->video_title = $request->video_title;
        $video->posted_date = $request->posted_date;
        $video->video_url = $request->video_url;
        $video->video_category_name = $request->category_name;
        $video->video_category_color = $request->category_color;
        if (strpos($request->video_url,'?v=')) {
            $arr = explode("?v=", $request->video_url);
            $video->video_url_id = $arr[1];
            $video->image_url = "https://img.youtube.com/vi/".$arr[1]."/maxresdefault.jpg";
        }
        $video->updated_by = $request->user()->admin_user_id;
        $video->save();

        return redirect()->route('videos.index')->with('message', '動画ID：'.$id.'の更新が完了しました');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // 動画削除
        $video = Video::find($id);
        $video->delete();

        return redirect()->route('videos.index')->with('message', '動画ID：'.$id.'の削除が完了しました');
    }
}

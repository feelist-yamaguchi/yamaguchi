<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\Information;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:cms');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // お知らせ一覧表示
        $informations = Information::orderBy('posted_date', 'desc')->paginate(10);
        return view('cms.information.index', [
            'informations' => $informations,
            'user' => $request->user(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // お知らせ登録画面表示
        return view('cms.information.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // お知らせ登録
        $information = new Information;
        $information->especial_flag = $request->especial_flag;
        $information->information_title = $request->information_title;
        $information->information_text = $request->information_text;
        $information->posted_date = $request->posted_date;
        $information->created_by = $request->user()->admin_user_id;
        $information->updated_by = $request->user()->admin_user_id;
        $information->save();

        return redirect()->route('information.index')->with('message', 'お知らせの登録が完了しました');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // お知らせ編集画面表示
        $information = Information::find($id);
        $information->posted_date = str_replace(" ", "T", $information->posted_date);
        return view('cms.information.update', [
            'information' => $information,
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // お知らせ更新
        $information = Information::find($id);
        if (($request->especial_flag) == 1 || ($request->especial_flag) == "on") {
            $information->especial_flag = 1;
        } else {
            $information->especial_flag = 0;
        }
        $information->information_title = $request->information_title;
        $information->information_text = $request->information_text;
        $information->posted_date = $request->posted_date;
        $information->updated_by = $request->user()->admin_user_id;
        $information->save();

        return redirect()->route('information.index')->with('message', 'お知らせID：'.$id.' の更新が完了しました');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // お知らせ削除
        $information = Information::find($id);
        $information->delete();

        return redirect()->route('information.index')->with('message', 'お知らせID：'.$id.' の削除が完了しました');
    }
}

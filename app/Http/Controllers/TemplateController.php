<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function index(Request $request)
    {
        // SEO
        $seo = (object) array(
            "title" => "ツーリングマップルWEB｜ツーリングライフをもっとオモシロくする、情報・交流サイト",
            "keywords" => "ツーリング,バイク,旅,地図,マップ,情報",
            "description" => "ツーリングマップルWEBは、ライダーのバイブル「ツーリングマップル」編集部が運営する、バイク旅をもっと面白くするための情報発信＆共有メディア。ユーザー同士の情報交換地図「みんなのツーリングマップル」をはじめ、ツーリングに出かけたくなるようなコラムや企画、最新の道路・スポット情報などをお届けします！",
//            "robots" => "index,follow",
//            "canonical" => "",
        );

        return view('blank', array('seo' => $seo));
    }

    public function index_b(Request $request)
    {
        // SEO
        $seo = (object) array(
            "title" => "ツーリングマップルWEB｜ツーリングライフをもっとオモシロくする、情報・交流サイト",
            "keywords" => "ツーリング,バイク,旅,地図,マップ,情報",
            "description" => "ツーリングマップルWEBは、ライダーのバイブル「ツーリングマップル」編集部が運営する、バイク旅をもっと面白くするための情報発信＆共有メディア。ユーザー同士の情報交換地図「みんなのツーリングマップル」をはじめ、ツーリングに出かけたくなるようなコラムや企画、最新の道路・スポット情報などをお届けします！",
//            "robots" => "index,follow",
//            "canonical" => "",
        );

        return view('blank_with_sidebar', array('seo' => $seo));
    }
}

<?php

namespace Database\Factories;

use App\Models\Touring_user;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class Touring_userFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Touring_user::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_name' => '佐藤太郎',
            'email' => 'test01@test.co.jp',
            'email_verified_flag' => 1,
            'password' => Hash::make('P@ssw0rd'),
            'token' => Str::random(60),
            'token_lifetime' => 3600,
            'refresh_token' => Str::random(60),
            'notification_flag' => 'notification_flag'
        ];
    }
}

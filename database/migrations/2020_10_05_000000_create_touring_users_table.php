<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTouringUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('touring_users', function (Blueprint $table) {
            $table->id('user_id')->unsigned()->comment('ユーザID');
            $table->string('user_name')->comment('ユーザ名（ニックネーム）');
            $table->string('email')->unique()->comment('メールアドレス');
            $table->boolean('email_verified_flag')->comment('メール確認フラグ');
            $table->string('password')->comment('パスワード');
            $table->longText('token')->comment('トークン');
            $table->string('token_lifetime')->comment('トークン有効期限');
            $table->string('refresh_token')->comment('リフレッシュトークン');
            $table->string('fcm_token')->nullable()->comment('FCMトークン');
            $table->dateTime('hokkaido_expiration_time')->nullable()->comment('北海道エリア利用期限');
            $table->dateTime('tohoku_expiration_time')->nullable()->comment('東北エリア利用期限');
            $table->dateTime('kanto_expiration_time')->nullable()->comment('関東甲信越エリア利用期限');
            $table->dateTime('chubu_expiration_time')->nullable()->comment('中部北陸エリア利用期限');
            $table->dateTime('kansai_expiration_time')->nullable()->comment('関西エリア利用期限');
            $table->dateTime('chugokushikoku_expiration_time')->nullable()->comment('中国四国エリア利用期限');
            $table->dateTime('kyushu_expiration_time')->nullable()->comment('九州沖縄エリア利用期限');
            $table->string('user_gender')->nullable()->comment('性別');
            $table->string('user_prefecture')->nullable()->comment('居住都道府県');
            $table->string('user_birthday')->nullable()->comment('生年月日');
            $table->string('user_area')->nullable()->comment('居住エリア');
            $table->longText('user_text')->nullable()->comment('自己紹介文');
            $table->text('icon_image_url')->nullable()->comment('アイコン画像URL');
            $table->text('twitter_url')->nullable()->comment('Twitter URL');
            $table->text('imstagram_url')->nullable()->comment('Imstagram URL');
            $table->text('facebook_url')->nullable()->comment('Facebook URL');
            $table->text('youtube_url')->nullable()->comment('YouTube URL');
            $table->text('website_url')->nullable()->comment('ウェブサイトURL');
            $table->string('notification_flag')->comment('Push通知受信');
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE touring_users COMMENT 'ユーザ'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('touring_users');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            $table->id('admin_user_id')->unsigned()->comment('管理者ユーザID');
            $table->string('name')->comment('ユーザ名');
            $table->string('email')->unique()->comment('メールアドレス');
            $table->timestamp('email_verified_at')->nullable()->comment('メール確認フラグ');
            $table->string('password')->comment('パスワード');
            //$table->string('token')->comment('トークン');
            //$table->dateTime('token_lifetime')->comment('トークン有効期限');
            //$table->string('refresh_token')->comment('リフレッシュトークン');
            $table->rememberToken()->comment('トークン');
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE admin_users COMMENT '管理者ユーザ'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_users');
    }
}

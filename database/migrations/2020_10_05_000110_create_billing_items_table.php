<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateBillingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_items', function (Blueprint $table) {
            $table->id('billing_items_id')->unsigned()->comment('課金アイテムID');
            $table->bigInteger('product_id')->unique()->comment('製品ID');
            $table->string('product_name')->comment('商品名');
            $table->string('os_code')->comment('対象OSコード');
            $table->timestamps();

        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE billing_items COMMENT '課金アイテム'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_items');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTouringLogsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('touring_logs_images', function (Blueprint $table) {
            $table->id('touring_log_image_id')->unsigned()->comment('旅の履歴写真ID');
            $table->bigInteger('touring_log_id')->unsigned()->comment('旅の履歴ID');
            $table->longText('image_url')->comment('画像URL');
            $table->text('image_text')->nullable()->comment('コメント本文');
            $table->timestamps();

            // foreign key
            $table->foreign('touring_log_id')->references('touring_log_id')->on('touring_logs')->onDelete('CASCADE');
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE touring_logs_images COMMENT '旅の履歴写真'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('touring_logs_images');
    }
}

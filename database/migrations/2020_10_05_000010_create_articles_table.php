<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id('article_id')->unsigned()->comment('記事ID');
            $table->text('article_title')->comment('記事タイトル');
            $table->longText('intro_text')->nullable()->comment('導入文');
            $table->dateTime('posted_date')->comment('投稿日時');
            $table->string('author_name')->nullable()->comment('著者名');
            $table->string('article_category_name')->comment('カテゴリ名');
            $table->string('article_category_color')->comment('カテゴリ色コード');
            $table->text('image_url')->nullable()->comment('サムネイル画像URL');
            $table->bigInteger('created_by')->unsigned()->comment('投稿者ID');
            $table->bigInteger('updated_by')->unsigned()->comment('更新者ID');
            $table->timestamps();

            // foreign key
            $table->foreign('created_by')->references('admin_user_id')->on('admin_users')->onDelete('CASCADE');
            $table->foreign('updated_by')->references('admin_user_id')->on('admin_users')->onDelete('CASCADE');

        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE articles COMMENT '記事コラム'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}

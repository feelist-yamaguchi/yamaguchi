<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->id('information_id')->unsigned()->comment('お知らせID');
            $table->text('information_title')->comment('お知らせタイトル');
            $table->longText('information_text')->comment('本文');
            $table->boolean('especial_flag')->comment('特別なお知らせフラグ');
            $table->dateTime('posted_date')->comment('投稿日時');
            $table->bigInteger('created_by')->unsigned()->comment('投稿者ID');
            $table->bigInteger('updated_by')->unsigned()->comment('更新者ID');
            $table->timestamps();

            // foreign key
            $table->foreign('created_by')->references('admin_user_id')->on('admin_users')->onDelete('CASCADE');
            $table->foreign('updated_by')->references('admin_user_id')->on('admin_users')->onDelete('CASCADE');

        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE information COMMENT 'お知らせ'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information');
    }
}

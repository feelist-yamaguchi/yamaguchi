<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateLogDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_details', function (Blueprint $table) {
            $table->id('log_id')->unsigned()->comment('ログID');
            $table->bigInteger('touring_log_id')->unsigned()->comment('旅の履歴ID');
            $table->dateTime('gps_date')->comment('GPS取得日時');
            $table->double('log_longitude', 20, 10)->comment('緯度');
            $table->double('log_latitude', 20, 10)->comment('経度');
            $table->timestamps();

            // foreign key
            $table->foreign('touring_log_id')->references('touring_log_id')->on('touring_logs')->onDelete('CASCADE');
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE log_details COMMENT '旅の履歴GPSログ'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id('video_id')->unsigned()->comment('動画ID');
            $table->text('video_title')->comment('動画タイトル');
            $table->dateTime('posted_date')->comment('投稿日時');
            $table->text('video_url')->comment('動画URL');
            $table->string('video_url_id')->nullable()->comment('動画URLのID');
            $table->string('video_category_name')->comment('カテゴリ名');
            $table->string('video_category_color')->comment('カテゴリ色コード');
            $table->longText('image_url')->nullable()->comment('サムネイル画像URL');
            $table->bigInteger('created_by')->unsigned()->comment('投稿者ID');
            $table->bigInteger('updated_by')->unsigned()->comment('更新者ID');
            $table->timestamps();
            
            // foreign key
            $table->foreign('created_by')->references('admin_user_id')->on('admin_users')->onDelete('CASCADE');
            $table->foreign('updated_by')->references('admin_user_id')->on('admin_users')->onDelete('CASCADE');
        });
        
        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE videos COMMENT '動画'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}

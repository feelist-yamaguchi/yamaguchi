<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateReceiptVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_verifications', function (Blueprint $table) {
            $table->id('receipt_verification_id')->unsigned()->comment('レシート検証ID');
            $table->bigInteger('billing_items_id')->unsigned()->comment('課金アイテムID');
            $table->bigInteger('receipt_id')->unsigned()->comment('レシートID');
            $table->string('result_code')->comment('検証結果コード');
            $table->string('os_code')->comment('対象OSコード');
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();

            // foreign key
            $table->foreign('billing_items_id')->references('billing_items_id')->on('billing_items')->onDelete('CASCADE');
            $table->foreign('receipt_id')->references('receipt_id')->on('receipt_information')->onDelete('CASCADE');

        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE receipt_verifications COMMENT 'レシート検証結果'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_verifications');
    }
}

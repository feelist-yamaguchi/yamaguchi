<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTouringLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('touring_logs', function (Blueprint $table) {
            $table->id('touring_log_id')->unsigned()->comment('旅の履歴ID');
            $table->bigInteger('user_id')->unsigned()->comment('ユーザID');
            $table->text('touring_log_title')->comment('タイトル');
            $table->longText('touring_log_body')->nullable()->comment('本文');
            $table->timestamps();

            // foreign key
            $table->foreign('user_id')->references('user_id')->on('touring_users')->onDelete('CASCADE');
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE touring_logs COMMENT 'ツーリングログ'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('touring_logs');
    }
}

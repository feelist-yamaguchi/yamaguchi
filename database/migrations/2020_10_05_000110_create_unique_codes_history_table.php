<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUniqueCodesHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unique_codes_history', function (Blueprint $table) {
            $table->id('unique_code_history_id')->unsigned()->comment('ユニークコード履歴ID');
            $table->bigInteger('user_id')->unsigned()->comment('ユーザID');
            $table->bigInteger('unique_code_id')->unsigned()->comment('ユニークコードID');
            $table->dateTime('end_time')->comment('有効期限日時');
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();

            // foreign key
            $table->foreign('user_id')->references('user_id')->on('touring_users')->onDelete('CASCADE');
            $table->foreign('unique_code_id')->references('unique_code_id')->on('unique_codes')->onDelete('CASCADE');
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE unique_codes_history COMMENT 'ユニークコード利用履歴'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unique_codes_history');
    }
}

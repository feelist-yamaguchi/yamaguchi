<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUniqueCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unique_codes', function (Blueprint $table) {
            $table->id('unique_code_id')->unsigned()->comment('ユニークコードID');
            $table->string('unique_code')->unique()->comment('ユニークコード');
            $table->bigInteger('user_id')->unsigned()->comment('ユーザID');
            $table->string('area_code')->unique()->comment('地図エリアコード');
            $table->string('public_code')->unique()->comment('出版物コード');
            $table->dateTime('start_time')->comment('利用開始日時');
            $table->dateTime('end_time')->comment('有効期限日時');
            $table->boolean('used_flag')->comment('使用済フラグ');
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();

            // foreign key
            $table->foreign('user_id')->references('user_id')->on('touring_users')->onDelete('CASCADE');
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE unique_codes COMMENT 'ユニークコード'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unique_codes');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionRelatedUrlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_related_url', function (Blueprint $table) {
            $table->id('related_url_id')->unsigned()->comment('関連URLID');
            $table->bigInteger('article_id')->unsigned()->comment('記事ID');
            $table->bigInteger('section_id')->unsigned()->comment('セクションID');
            $table->text('related_url_title')->nullable()->comment('関連URLタイトル');
            $table->text('related_url')->comment('関連URL');
            $table->timestamps();
            
            // foreign key
            $table->foreign('article_id')->references('article_id')->on('articles')->onDelete('CASCADE');
            $table->foreign('section_id')->references('section_id')->on('article_sections')->onDelete('CASCADE');
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE section_related_url COMMENT '記事セクション関連URL'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_related_url');
    }
}

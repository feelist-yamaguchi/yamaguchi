<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateArticleSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_sections', function (Blueprint $table) {
            $table->id('section_id')->unsigned()->comment('セクションID');
            $table->bigInteger('article_id')->unsigned()->comment('記事ID');
            $table->text('article_title')->comment('セクションタイトル');
            $table->longText('article_body')->comment('セクション本文');
            $table->Integer('image_number')->comment('画像数');
            $table->text('image_url1')->nullable()->comment('画像URL1');
            $table->text('image_url2')->nullable()->comment('画像URL2');
            $table->text('image_url3')->nullable()->comment('画像URL3');
            $table->text('image_url4')->nullable()->comment('画像URL4');
            $table->text('image_url5')->nullable()->comment('画像URL5');
            $table->timestamps();
            
            // foreign key
            $table->foreign('article_id')->references('article_id')->on('articles')->onDelete('CASCADE');
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE article_sections COMMENT '記事コラムセクション'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_sections');
    }
}

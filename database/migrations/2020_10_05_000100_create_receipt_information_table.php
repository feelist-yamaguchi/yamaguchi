<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateReceiptInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_information', function (Blueprint $table) {
            $table->id('receipt_id')->unsigned()->comment('レシートID');
            $table->bigInteger('user_id')->unsigned()->comment('ユーザID');
            $table->string('product_name')->comment('レシート情報');
            $table->string('os_code')->comment('対象OSコード');
            $table->dateTime('expiration_time')->comment('有効期限日時');
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();

            // foreign key
            $table->foreign('user_id')->references('user_id')->on('touring_users')->onDelete('CASCADE');
        });

        if (env('APP_ENV') !== 'testing' && env('DB_CONNECTION') === 'mysql' ) {
            DB::statement("ALTER TABLE receipt_information COMMENT 'レシート情報'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_information');
    }
}

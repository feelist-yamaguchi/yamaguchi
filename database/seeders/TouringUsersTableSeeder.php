<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class TouringUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('touring_users')->truncate();

        // touring_usersテーブルの初期作成(30件)
        $touring_users = [
            [
                'user_name' => '佐藤太郎',
                'email' => 'test01@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '田中一郎',
                'email' => 'test02@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '山田花子',
                'email' => 'test03@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '高橋健太',
                'email' => 'test04@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '鈴木裕司',
                'email' => 'test05@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '佐藤太郎',
                'email' => 'test06@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '田中一郎',
                'email' => 'test07@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '山田花子',
                'email' => 'test08@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '高橋健太',
                'email' => 'test09@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '鈴木裕司',
                'email' => 'test10@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '佐藤太郎',
                'email' => 'test11@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '田中一郎',
                'email' => 'test12@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '山田花子',
                'email' => 'test13@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '高橋健太',
                'email' => 'test14@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '鈴木裕司',
                'email' => 'test15@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '佐藤太郎',
                'email' => 'test16@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '田中一郎',
                'email' => 'test17@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '山田花子',
                'email' => 'test18@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '高橋健太',
                'email' => 'test19@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '鈴木裕司',
                'email' => 'test20@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '佐藤太郎',
                'email' => 'test21@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '田中一郎',
                'email' => 'test22@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '山田花子',
                'email' => 'test23@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '高橋健太',
                'email' => 'test24@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '鈴木裕司',
                'email' => 'test25@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '佐藤太郎',
                'email' => 'test26@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '田中一郎',
                'email' => 'test27@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '山田花子',
                'email' => 'test28@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '高橋健太',
                'email' => 'test29@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ],
            [
                'user_name' => '鈴木裕司',
                'email' => 'test30@test.co.jp',
                'email_verified_flag' => 1,
                'password' => Hash::make('P@ssw0rd'),
                'token' => Str::random(60),
                'token_lifetime' => '2021-04-01 12:00:00',
                'refresh_token' => Str::random(60),
                'notification_flag' => 'notification_flag'
            ]
        ];

        // DB登録
        foreach ($touring_users as $touring_user) {
            DB::table('touring_users')->insert($touring_user);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

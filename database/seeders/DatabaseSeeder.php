<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // ユーザテーブル
        $this->call(TouringUsersTableSeeder::class);
        // ツーリングログテーブル
        $this->call(TouringLogsTableSeeder::class);
        // 旅の履歴GPSログテーブル
        $this->call(LogDetailsTableSeeder::class);
        // 旅の履歴写真テーブル
        $this->call(TouringLogsImagesTableSeeder::class);
        // ユニークコードテーブル
        $this->call(UniqueCodesTableSeeder::class);
        // ユニークコード利用履歴テーブル
        $this->call(UniqueCodesHistoryTableSeeder::class);
        // 記事コラムテーブル
        $this->call(ArticlesTableSeeder::class);
        // 課金アイテムテーブル
        $this->call(BillingItemsTableSeeder::class);
        // 動画テーブル
        $this->call(VideosTableSeeder::class);
        // 管理者ユーザテーブル
        $this->call(AdminUsersTableSeeder::class);
        // お知らせテーブル
        $this->call(InformationTableSeeder::class);
    }
}

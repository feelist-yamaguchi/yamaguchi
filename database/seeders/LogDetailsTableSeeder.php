<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class LogDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('log_details')->truncate();

        $log_details= [
            ['touring_log_id' => 1,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 2,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 3,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 4,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 5,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 6,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 7,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 8,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 9,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 10,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 11,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 12,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 13,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 14,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0],
            ['touring_log_id' => 15,
            'gps_date' => '2020-10-14 12:00:00',
            'log_longitude' => 35.0,
            'log_latitude' => 135.0]
        ];

        // DB登録
        foreach ($log_details as $log_detail) {
            DB::table('log_details')->insert($log_detail);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

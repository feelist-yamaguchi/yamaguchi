<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TouringLogsImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('touring_logs_images')->truncate();

        $touring_logs_images = [
            ['touring_log_id' => 1,
            'image_url' => 'image_url'],
            ['touring_log_id' => 2,
            'image_url' => 'image_url'],
            ['touring_log_id' => 3,
            'image_url' => 'image_url'],
            ['touring_log_id' => 4,
            'image_url' => 'image_url'],
            ['touring_log_id' => 5,
            'image_url' => 'image_url'],
            ['touring_log_id' => 6,
            'image_url' => 'image_url'],
            ['touring_log_id' => 7,
            'image_url' => 'image_url'],
            ['touring_log_id' => 8,
            'image_url' => 'image_url'],
            ['touring_log_id' => 9,
            'image_url' => 'image_url'],
            ['touring_log_id' => 10,
            'image_url' => 'image_url']
        ];
        // DB登録
        foreach ($touring_logs_images as $touring_logs_image) {
            DB::table('touring_logs_images')->insert($touring_logs_image);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TouringLogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('touring_logs')->truncate();

        $touring_logs = [
            ['user_id' => 1,
            'touring_log_title' => 'log1'],
            ['user_id' => 1,
            'touring_log_title' => 'log2'],
            ['user_id' => 1,
            'touring_log_title' => 'log3'],
            ['user_id' => 1,
            'touring_log_title' => 'log4'],
            ['user_id' => 1,
            'touring_log_title' => 'log5'],
            ['user_id' => 2,
            'touring_log_title' => 'log6'],
            ['user_id' => 2,
            'touring_log_title' => 'log7'],
            ['user_id' => 2,
            'touring_log_title' => 'log8'],
            ['user_id' => 2,
            'touring_log_title' => 'log9'],
            ['user_id' => 2,
            'touring_log_title' => 'log10'],
            ['user_id' => 3,
            'touring_log_title' => 'log11'],
            ['user_id' => 3,
            'touring_log_title' => 'log12'],
            ['user_id' => 3,
            'touring_log_title' => 'log13'],
            ['user_id' => 3,
            'touring_log_title' => 'log14'],
            ['user_id' => 3,
            'touring_log_title' => 'log15'],
            ['user_id' => 4,
            'touring_log_title' => 'log16'],
            ['user_id' => 5,
            'touring_log_title' => 'log17'],
            ['user_id' => 6,
            'touring_log_title' => 'log18'],
            ['user_id' => 7,
            'touring_log_title' => 'log19'],
            ['user_id' => 8,
            'touring_log_title' => 'log20'],
            ['user_id' => 9,
            'touring_log_title' => 'log21'],
            ['user_id' => 10,
            'touring_log_title' => 'log22'],
            ['user_id' => 11,
            'touring_log_title' => 'log23'],
            ['user_id' => 12,
            'touring_log_title' => 'log24'],
            ['user_id' => 13,
            'touring_log_title' => 'log25'],
            ['user_id' => 14,
            'touring_log_title' => 'log26'],
            ['user_id' => 15,
            'touring_log_title' => 'log27']
        ];

        // DB登録
        foreach ($touring_logs as $touring_log) {
            DB::table('touring_logs')->insert($touring_log);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

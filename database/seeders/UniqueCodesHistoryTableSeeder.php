<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UniqueCodesHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('unique_codes_history')->truncate();
        
        $unique_codes_histories = [
            ['user_id' => 1,
            'unique_code_id' => 1,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 2,
            'unique_code_id' => 2,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 3,
            'unique_code_id' => 3,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 4,
            'unique_code_id' => 4,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 5,
            'unique_code_id' => 5,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 6,
            'unique_code_id' => 6,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 7,
            'unique_code_id' => 7,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 8,
            'unique_code_id' => 8,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 9,
            'unique_code_id' => 9,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 10,
            'unique_code_id' => 10,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 11,
            'unique_code_id' => 11,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 12,
            'unique_code_id' => 12,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 13,
            'unique_code_id' => 13,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 14,
            'unique_code_id' => 14,
            'end_time' => '2021-10-10 11:59:59'],
            ['user_id' => 15,
            'unique_code_id' => 15,
            'end_time' => '2021-10-10 11:59:59']
        ];
        // DB登録
        foreach ($unique_codes_histories as $unique_codes_history) {
            DB::table('unique_codes_history')->insert($unique_codes_history);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('videos')->truncate();

        $videos = [
            [
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',   
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],[
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],[
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],[
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],[
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],[
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],[
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],[
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],[
                'video_title' => 'Meet iPhone 12 — Apple',
                'posted_date' => '2020-10-14 05:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=65JrtwtTOdc',
                'video_url_id' => '65JrtwtTOdc',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/65JrtwtTOdc/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Building digital resilience | Microsoft CEO Satya Nadella',
                'posted_date' => '2020-10-13 12:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=FoIOFEp2HiY',
                'video_url_id' => 'FoIOFEp2HiY',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/FoIOFEp2HiY/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'video_title' => 'Plus Codes: A Navajo Nation Collaboration',
                'posted_date' => '2020-10-12 16:00:00',
                'video_url' => 'https://www.youtube.com/watch?v=vn9jlpHCxBU',
                'video_url_id' => 'vn9jlpHCxBU',
                'video_category_name' => "001",
                'video_category_color' => '#fff',
                'image_url' => 'https://img.youtube.com/vi/vn9jlpHCxBU/maxresdefault.jpg',
                'created_by' => 1,
                'updated_by' => 1
            ]
        ];

        // DB登録
        foreach ($videos as $video) {
            DB::table('videos')->insert($video);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

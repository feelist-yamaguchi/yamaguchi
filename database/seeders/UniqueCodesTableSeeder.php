<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UniqueCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('unique_codes')->truncate();

        $unique_codes = [
            ['unique_code' => Str::random(20),
            'user_id' => 1,
            'area_code' => '001',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 2,
            'area_code' => '002',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 3,
            'area_code' => '003',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 4,
            'area_code' => '004',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 5,
            'area_code' => '005',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 6,
            'area_code' => '006',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 7,
            'area_code' => '007',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 8,
            'area_code' => '008',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 9,
            'area_code' => '009',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 10,
            'area_code' => '010',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 11,
            'area_code' => '011',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 12,
            'area_code' => '012',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 13,
            'area_code' => '013',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 14,
            'area_code' => '014',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1],
            ['unique_code' => Str::random(20),
            'user_id' => 15,
            'area_code' => '015',
            'public_code' => Str::random(20),
            'start_time' => '2020-10-10 12:00:00',
            'end_time' => '2021-10-10 11:59:59',
            'used_flag' => 1]
        ];
        
        // DB登録
        foreach ($unique_codes as $unique_code) {
            DB::table('unique_codes')->insert($unique_code);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class BillingItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('billing_items')->truncate();

        $billing_items = [
            [
                'product_id' => 001,
                'product_name' => '1エリア課金',
                'os_code' => '001'
            ],
            [
                'product_id' => 002,
                'product_name' => '3エリア課金',
                'os_code' => '001'
            ],
            [
                'product_id' => 003,
                'product_name' => '全エリア課金',
                'os_code' => '001'
            ]
        ];

        // DB登録
        foreach ($billing_items as $billing_item) {
            DB::table('billing_items')->insert($billing_item);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

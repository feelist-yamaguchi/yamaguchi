<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('admin_users')->truncate();
        
        $admin_users = [
            [
                'name' => '昭文社管理者',
                'email' => 'shobunsha01@mapple.co.jp',
                'email_verified_at' => now(),
                'password' => Hash::make('shobunsha01'),
                'remember_token' => Str::random(10),
            ],
            [
                'name' => '昭文社管理者02',
                'email' => 'shobunsha02@mapple.co.jp',
                'email_verified_at' => now(),
                'password' => Hash::make('shobunsha02'),
                'remember_token' => Str::random(10),
            ]
        ];

        // DB登録
        foreach ($admin_users as $admin_user) {
            DB::table('admin_users')->insert($admin_user);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

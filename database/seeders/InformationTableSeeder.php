<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 外部キー制約を無効化
        Schema::disableForeignKeyConstraints();
        // テーブルのクリア
        DB::table('information')->truncate();

        $informations = [
            [
                'information_title' => 'お知らせ1',
                'information_text' => 'お知らせ1',
                'especial_flag' => 0,
                'posted_date' => '2020-10-01 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ2',
                'information_text' => 'お知らせ2',
                'especial_flag' => 0,
                'posted_date' => '2020-10-02 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ3',
                'information_text' => 'お知らせ3',
                'especial_flag' => 0,
                'posted_date' => '2020-10-03 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ4',
                'information_text' => 'お知らせ4',
                'especial_flag' => 0,
                'posted_date' => '2020-10-04 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ5',
                'information_text' => 'お知らせ5',
                'especial_flag' => 0,
                'posted_date' => '2020-10-05 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ6',
                'information_text' => 'お知らせ6',
                'especial_flag' => 0,
                'posted_date' => '2020-10-06 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ7',
                'information_text' => 'お知らせ7',
                'especial_flag' => 0,
                'posted_date' => '2020-10-07 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ8',
                'information_text' => 'お知らせ8',
                'especial_flag' => 0,
                'posted_date' => '2020-10-08 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ9',
                'information_text' => 'お知らせ9',
                'especial_flag' => 0,
                'posted_date' => '2020-10-09 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ10',
                'information_text' => 'お知らせ10',
                'especial_flag' => 0,
                'posted_date' => '2020-10-10 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ11',
                'information_text' => 'お知らせ11',
                'especial_flag' => 0,
                'posted_date' => '2020-10-11 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ12',
                'information_text' => 'お知らせ12',
                'especial_flag' => 0,
                'posted_date' => '2020-10-12 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ13',
                'information_text' => 'お知らせ13',
                'especial_flag' => 0,
                'posted_date' => '2020-10-13 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ14',
                'information_text' => 'お知らせ14',
                'especial_flag' => 0,
                'posted_date' => '2020-10-14 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ15',
                'information_text' => 'お知らせ15',
                'especial_flag' => 0,
                'posted_date' => '2020-10-15 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-01 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-02 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-03 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-04 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-05 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-06 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-07 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-08 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-09 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-10 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-11 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-12 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-13 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-14 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ],
            [
                'information_title' => 'お知らせ',
                'information_text' => 'メンテナンスのお知らせ',
                'especial_flag' => 1,
                'posted_date' => '2020-10-15 12:00:00',
                'created_by' => 1,
                'updated_by' => 1
            ]
        ];

        // DB登録
        foreach ($informations as $information) {
            DB::table('information')->insert($information);
        }
        // 外部キー制約を有効化
        Schema::enableForeignKeyConstraints();
    }
}

<?php

namespace Tests\Feature;

use App\Models\Touring_user;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginApiTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // テストユーザー作成
        $this->touring_user = Touring_user::factory()->create();

    }

    /**
     * @test
     */
    public function Should_登録済みのユーザーを認証して返却する()
    {
        $response = $this->json('POST', route('user.login'), [
            'email' => $this->touring_user->email,
            'password' => 'P@ssw0rd',
        ]);


        $response
            ->assertStatus(200)
            ->assertJson([
                'user_name' => $this->touring_user->user_name
            ]);

        $this->assertAuthenticatedAs($this->touring_user);
    }

    /**
     * @test
     */
    public function Failed_メールアドレスが違う場合はエラーを返す()
    {
        $response = $this->json('POST', route('user.login'), [
            'email' => 'error@test.co.jp',
            'password' => 'P@ssw0rd',
        ]);


        $response
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Unauthorized'
            ]);

    }

    /**
     * @test
     */
    public function Failed_パスワードが違う場合はエラーを返す()
    {
        $response = $this->json('POST', route('user.login'), [
            'email' => $this->touring_user->email,
            'password' => 'error',
        ]);


        $response
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Unauthorized'
            ]);

    }


    /**
     * @test
     */
    public function Failed_パスワードとメールアドレスが違う場合はエラーを返す()
    {
        $response = $this->json('POST', route('user.login'), [
            'email' => 'error',
            'password' => 'error',
        ]);


        $response
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Unauthorized'
            ]);

    }

}

@section('footer')
    <footer id="GlobalFooter">
        <nav class="navbar navbar-light bg-secondary d-flex justify-content-center">
            <div class="center">
                <a class="nav-link active" href="/about">What is Sample</a>
                <a class="nav-link" href="/privacy_policy">Privacy policy</a>
                <a class="nav-link" href="/terms">Terms of service</a>
            </div>
            <div class="center">
                <a class="nav-link active" href="/about">About</a>
                <a class="nav-link" href="/link">Other Link</a>
                <a class="nav-link" href="mailto:hisaxxxxxx@gmail.com">Contact Us</a>
            </div>
        </nav>

        <nav class="navbar navbar-light bg-white d-flex justify-content-center">
            <img src="https://cdn.svgporn.com/logos/prismic-icon.svg" width="30px" height="30px" class="d-inline-block align-top mr-2" alt="">
            <a>Copyright © Sample ALL RIGHT RESERVED</a>
        </nav>
    </footer>
@endsection

<!DOCTYPE html>
<html lang="ja">
<head>
    @yield('head')
</head>
<body>
@yield('header')
@yield('breadcrumbs')
<div class="container mt-3 mb-3">
    <div class="row ">
        <div class="col-md-2 d-none d-md-block">
            @yield('sidebar_left')
        </div>
        <div class="col-md-8">
            @yield('contents')
        </div>
        <div class="col-md-2 d-none d-md-block">
            @yield('twitter_timeline')
            @yield('sidebar_right')
        </div>
    </div>
</div>
@yield('footer')
@yield('scripts')
</body>
</html>

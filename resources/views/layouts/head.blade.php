@section('head')
    <meta name="format-detection" content="telephone=no">
    @isset($seo->title)
        <title>{{ $seo->title }}</title>
    @endisset
    @empty($seo->title)
        <title>{{ config('app.name', 'Laravel') }}</title>
    @endempty
    @isset($seo->robots)
        <meta name="robots" content="{{ $seo->robots }}"/>
    @endisset
    @empty($seo->robots)
        <meta name="robots" content="index,follow"/>
    @endempty
    @isset($seo->keywords)
        <meta name="keywords" content="{{ $seo->keywords }}"/>
    @endisset
    @isset($seo->description)
        <meta name="description" content="{{ $seo->description }}"/>
    @endisset
    <meta name="author" content="Copyright © Sample All Rights Reserved."/>
    <meta property="og:locale" content="ja_jp"/>
    <link rel="icon" href="/img/favicon.ico"/>
    @isset($seo->canonical)
        <link rel="canonical" href="{{ $seo->canonical }}">
    @endisset
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('stylesheets')
@endsection

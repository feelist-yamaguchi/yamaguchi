@section('twitter_timeline')
    <h3 class="text-center">Twitter</h3>
    <a class="twitter-timeline" data-lang="ja" data-height="400" href="https://twitter.com/touringmapple_s?ref_src=twsrc%5Etfw">Tweets by touringmapple_s</a>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endsection

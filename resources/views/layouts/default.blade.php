<!DOCTYPE html>
<html lang="ja">
<head>
    @yield('head')
</head>
<body>
@yield('header')
@yield('breadcrumbs')
<div class="container mt-3 mb-3">
    @yield('contents')
    @yield('sidebar')
</div>
@yield('footer')
@yield('scripts')
</body>
</html>

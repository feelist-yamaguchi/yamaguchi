@extends('layouts.default')


@section('contents')
<form action="SignUpController.php" method="post">
  <div class="form">
    <table>
      <tbody>
        <tr>
          <th>メールアドレス<span class="nec">※</span></th>
          <td><input type="text" name="mail" size="30" maxlength="20"></td>
        </tr>
        <tr>
          <th>パスワード<span class="nec">※</span></th>
          <td><input type="text" name="password" size="30" maxlength="20"></td>
        </tr>
        <tr>
          <th>ユーザ名<span class="nec">※</span></th>
          <td><input type="text" name="name" size="30" maxlength="20"></td>
        </tr>
        <tr>
          <th>性別<span class="nec">※</span></th>
          <td><div>
              <input type="radio" name="gender" value="male">男性
              <input type="radio" name="gender" value="female">女性
              <input type="radio" name="gender" value="another">その他</div></td>
        </tr>
        <tr>
          <th>居住地<span class="nec">※</span></th>
          <td><input type="text" name="adress" size="30" maxlength="20"></td>
        </tr>
        <tr>
          <th>生年月日<span class="nec">※</span></th>
          <td><input type="text" name="birthday" size="30" maxlength="20"></td>
        </tr>
        <tr>
          <th>プロフィール画像</th>
          <td><input type="text" name="prof_img" size="30" maxlength="20"></td>
        </tr>
        <tr>
          <th>お気に入りエリア</th>
          <td><input type="text" name="area" size="30" maxlength="20"></td>
        </tr>
        <tr>
          <th>好きなツーリング</th>
          <td><input type="text" name="touring" size="30" maxlength="20"></td>
        </tr>
        <tr>
          <th>URL</th>
          <td><input type="text" name="url" size="30" maxlength="20"></td>
        </tr>
      </tbody>
    </table>
    <p class="sbmt">
      <input type="submit" value="確認画面へ" />
    </p>
  </div>
</form>
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
@endsection
 
@section('scripts')
    <script src="{{ asset('/js/app.js?now=@now()') }}"></script>
@endsection

@include('layouts.head')
@include('layouts.header')
@include('layouts.footer')
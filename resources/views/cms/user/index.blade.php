@extends('adminlte::page')

@section('title', 'ツーリングマップルCMS')

@section('content_header')
  <h1>ユーザ一覧</h1>
@stop

@section('content')
@if (Session::has('message'))
<div class="alert alert-success">
  {{ session('message') }}
</div>
@endif
  <div class="text-center">
      <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>ユーザID</th>
              <th>ユーザ名</th>
              <th>メールアドレス</th>
              <th>登録日</th>
              <th></th>
            </tr>
          </thead>
          @foreach ($touring_users as $user)
          <tbody>
            <tr>
              <td>{{ $user->user_id }}</td>
              <td>{{ $user->user_name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->created_at }}</td>
              <td>
                  <button type="button" class="btn btn-outline-primary" onclick="location.href='/touring/user/{{ $user->user_id }}/edit'">変更</button>
              </td>
            </tr>
          </tbody>
          @endforeach
      </table>
      {{ $touring_users->links('vendor.pagination.bootstrap-4') }}
  </div>
@stop

@section('css')
  <link rel="stylesheet" href="/css/cms.css">
@stop

@section('js')
  <script> console.log('Hi!'); </script>
@stop

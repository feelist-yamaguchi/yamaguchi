@extends('adminlte::page')

@section('title', 'ツーリングマップルCMS')

@section('content_header')
    <h1 class="text-center">ユーザステータス変更</h1>
@stop

@section('content')
    <center>
        <div class>
            <form action="/touring/user/{{ $touring_user->user_id }}" method="POST">
                @csrf
                @method('PUT')
                <table width="80%">
                    <tr>
                        <th>課金</th>
                        <td>{{ $billing ? 'あり' : 'なし'}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>利用期限</th>
                        <td>北海道エリア</td>
                        <td>
                            <input type="date" name="hokkaido_expiration_time" value="{{$touring_user->hokkaido_expiration_time}}">
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>東北エリア</td>
                        <td>
                            <input type="date" name="tohoku_expiration_time" value="{{$touring_user->tohoku_expiration_time}}">
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>関東甲信越エリア</td>
                        <td>
                            <input type="date" name="kanto_expiration_time" value="{{$touring_user->kanto_expiration_time}}">
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>中部北陸エリア</td>
                        <td>
                            <input type="date" name="chubu_expiration_time" value="{{$touring_user->chubu_expiration_time}}">
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>関西エリア</td>
                        <td>
                            <input type="date" name="kansai_expiration_time" value="{{$touring_user->kansai_expiration_time}}">
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>中国四国エリア</td>
                        <td>
                            <input type="date" name="chugokushikoku_expiration_time" value="{{$touring_user->chugokushikoku_expiration_time}}">
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>九州沖縄エリア</td>
                        <td>
                            <input type="date" name="kyushu_expiration_time" value="{{$touring_user->kyushu_expiration_time}}">
                        </td>
                    </tr>
                </table>
                <input type="submit" value="変更">
                
            </form>
            <button type="button" class="btn btn-sm btn-outline-dark" onclick="location.href='/touring/user'">戻る</buttton>
        </div>
    </center>
@stop

@section('css')
    <link rel="stylesheet" href="/css/cms.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

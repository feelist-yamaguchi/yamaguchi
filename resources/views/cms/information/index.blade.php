@extends('adminlte::page')

@section('title', 'ツーリングマップルCMS')

@section('content_header')
    <h1>お知らせ</h1>
@stop

@section('content')
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8 d-flex align-items-center"><h2 class="card-title">お知らせ一覧</h2></div>
                <div class="col-md-4 text-right"><button type="button" class="btn btn-primary" onclick="location.href='/information/create'">新規作成</buttton></div>
            </div>
        </div>
        <div class="card-body">
            <div class="text-right mb-2">
                <label>並び順：投稿日順</label>
            </div>
            @foreach($informations as $information)
                <div class="row pt-3 mb-3 border-top">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <p>お知らせID</p>
                            </div>
                            <div class="col-md-10">
                                <p>:&ensp;{{ $information->information_id }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <p>タイトル</p>
                            </div>
                            <div class="col-md-10">
                                @if ($information->especial_flag)
                                    <p>:&ensp;<span class= "tag">特別</span>&ensp;{{ $information->information_title }}</p>
                                @else
                                    <p>:&ensp;{{ $information->information_title }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <p>本文</p>
                            </div>
                            <div class="col-md-10">
                                <p class="information_text">:&ensp;{{ $information->information_text }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row col-md-6">
                                <div class="col-4">
                                    <p>投稿日時</p>
                                </div>
                                <div class="col-8">
                                    <p>:&ensp;{{ $information->posted_date }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="btn btn-success mr-3 update_btn" onclick="location.href='/information/{{ $information->information_id }}/edit'">編集</buttton>
                                <form id="information_delete_{{ $information->information_id }}" method="POST" action="/information/{{ $information->information_id }}">
                                    @method('DELETE')
                                    @csrf
                                    <button id="information_{{ $information->information_id }}" type="button" class="btn btn-danger delete_btn">削除</buttton>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row d-flex align-items-center justify-content-center">
                {{ $informations->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/cms.css">
@stop

@section('js')
    <script>
        $('.delete_btn').on('click', function() {
            let id = $(this).attr('id').split('_');
            if (confirm("お知らせid：" + id[1] + "のお知らせを削除します。よろしいですか？")) {
                $('#information_delete_' + id[1]).submit();
            } else {
                return false;
            }
        });
    </script>
@stop

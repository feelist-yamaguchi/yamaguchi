@extends('adminlte::page')

@section('title', 'ツーリングマップルCMS')

@section('content_header')
    <h1>お知らせ更新</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form method="POST" action="/information/{{ $information->information_id }}">
                @method('PUT')
                @csrf
                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="especial_flag">特別なお知らせ</label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input position-static especial_flag" name="especial_flag" @if ($information->especial_flag == 1) checked="checked" @endif>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="information_title">お知らせタイトル</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="information_title" name="information_title" value="{{ $information->information_title }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="information_text">お知らせ本文</label>
                    </div>
                    <div class="col-md-10">
                        <textarea class="form-control" name="information_text" rows="10" required>{{ $information->information_text }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="posted_date">投稿日時</label>
                    </div>
                    <div class="col-md-10">
                        <input type="datetime-local" name="posted_date" value="{{ $information->posted_date }}" required>
                    </div>
                </div>
                <div class="mt-4 mb-4 text-center">
                    <input type="submit" class="btn btn-primary" value="更新する">
                </div>
            </form>
            <div class="mt-4">
                <button type="button" class="btn btn-info" onclick="location.href='/information'">一覧に戻る</button>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/cms.css">
@stop

@section('js')
    <script>
        $(".especial_flag").on("click", function(){
            if($(this).prop("checked") == true){
                $('.especial_flag').val(1);
            }else{
                $('.especial_flag').val(0);
            }
        });
    </script>
@stop
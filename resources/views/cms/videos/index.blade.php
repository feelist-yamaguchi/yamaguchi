@extends('adminlte::page')

@section('title', 'ツーリングマップルCMS')

@section('content_header')
    <h1>動画</h1>
@stop

@section('content')
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8 d-flex align-items-center"><h2 class="card-title">動画一覧</h2></div>
                <div class="col-md-4 text-right"><button type="button" class="btn btn-primary" onclick="location.href='/videos/create'">新規作成</buttton></div>
            </div>
        </div>
        <div class="card-body">
            <div class="text-right mb-2">
                <label>並び順：投稿日順</label>
            </div>  
            @foreach($videos as $video)
                <div class="row pt-3 mb-3 border-top">
                    <div class="col-md-3">
                        <a href="{{ $video->video_url }}" target="_blank" rel="noopener noreferrer"><img src="{{ $video->image_url }}" width="100%"></a>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-2">
                                <p>動画ID</p>
                            </div>
                            <div class="col-md-10">
                                <p>:&ensp;{{ $video->video_id }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <p>動画タイトル</p>
                            </div>
                            <div class="col-md-10">
                                <p>:&ensp;<a href="{{ $video->video_url }}" target="_blank" rel="noopener noreferrer">{{ $video->video_title }}</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row col-md-6">
                                <div class="col-4">
                                    <p>カテゴリ</p>
                                </div>
                                <div class="col-8">
                                    <p>:&ensp;{{ $video->video_category_name }}</p>
                                </div>
                            </div>
                            <div class="row col-md-6">
                                <div class="col-5">
                                    <p>カテゴリ色</p>
                                </div>
                                <div class="col-7">
                                    <p>:&ensp;{{ $video->video_category_color }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row col-md-6">
                                <div class="col-4">
                                    <p>投稿日時</p>
                                </div>
                                <div class="col-8">
                                    <p>:&ensp;{{ $video->posted_date }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="btn btn-success mr-3 update_btn" onclick="location.href='/videos/{{ $video->video_id }}/edit'">編集</buttton>
                                <form id="video_delete_{{ $video->video_id }}" method="POST" action="/videos/{{ $video->video_id }}">
                                    @method('DELETE')
                                    @csrf
                                    <button id="video_{{ $video->video_id }}" type="button" class="btn btn-danger delete_btn">削除</buttton>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row d-flex align-items-center justify-content-center">
                {{ $videos->links('vendor.pagination.bootstrap-4') }}
            </div>    
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/cms.css">
@stop

@section('js')
    <script>
        $('.delete_btn').on('click', function() {
            let id = $(this).attr('id').split('_');
            if (confirm("動画id：" + id[1] + "の動画を削除します。よろしいですか？")) {
                $('#video_delete_' + id[1]).submit();
            } else {
                return false;
            }
        });
    </script>
@stop

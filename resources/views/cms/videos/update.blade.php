@extends('adminlte::page')

@section('title', 'ツーリングマップルCMS')

@section('content_header')
    <h1>動画更新</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form method="POST" action="/videos/{{ $video->video_id }}">
                @method('PUT')
                @csrf
                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="video_title">動画タイトル</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="video_title" name="video_title" value="{{ $video->video_title }}" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="category_name">カテゴリ名</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="category_name" value="{{ $video->video_category_name }}" required>
                    </div>
                    <div class="col-md-2">
                        <label for="category_color">カテゴリ色</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="category_color" value="{{ $video->video_category_color }}" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="posted_date">投稿日時</label>
                    </div>
                    <div class="col-md-6">
                        <input type="datetime-local" name="posted_date" value="{{ $video->posted_date }}" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="video_url">動画URL</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="video_url" name="video_url" value="{{ $video->video_url }}" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                        <label class="mb-3 mr-3" for="image_url">サムネイル画像</label>
                        <button type="button" class="btn btn-primary" id="show_thumbnail">サムネイル表示</button>
                    </div>
                    <div class="col-md-6 d-flex align-items-center justify-content-center thumbnail_image">
                        <img src="{{ $video->image_url }}" height="100%">
                    </div>
                </div>

                <div class="mt-4 mb-4 text-center">
                    <input type="submit" class="btn btn-primary" value="更新する">
                </div>
            </form>
            <div class="mt-4">
                <button type="button" class="btn btn-info" onclick="location.href='/videos'">一覧に戻る</button>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/cms.css">
@stop

@section('js')
    <script>
        $('#show_thumbnail').on('click', function() {
            video_url = $('.video_url').val();
            if (video_url.indexOf('?v=') !== -1) {
                $('.thumbnail_image').children().remove();
                video_id = video_url.split("?v=");
                imghtml = '<img src="https://img.youtube.com/vi/'+ video_id[1] +'/maxresdefault.jpg" height="100%">'
                $('.thumbnail_image').append(imghtml);
            } else {
                alert("YouTubeの動画URLが正しく入力されていません");
            }
        });
    </script>
@stop

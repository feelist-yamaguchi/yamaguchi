@extends('adminlte::page')

@section('title', 'ツーリングマップルCMS')

@section('content_header')
    <h1>MENU</h1>
@stop

@section('content')
    <div class="row mt-4 mb-4">
        <div class ="col-md-4 mb-2">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='/articles'">記事コラム</buttton>
        </div>
        <div class ="col-md-4 mb-2">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='/videos'">動画</buttton>
        </div>
        <div class ="col-md-4 mb-2">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='/stamprallies'">スタンプラリー</buttton>
        </div>
    </div>
    <div class="row mt-4 mb-4">
        <div class ="col-md-4 mb-2">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='/information'">お知らせ</buttton>
        </div>
        <div class ="col-md-4 mb-2">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='/push_notifications'">Push通知</buttton>
        </div>
        <div class ="col-md-4 mb-2">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='/touring/logs'">ログ管理</buttton>
        </div>
    </div>
    <div class="row mt-4 mb-4">
        <div class ="col-md-4 mb-2">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='/touring/user'">ユーザ管理</buttton>
        </div>
        <div class ="col-md-4 mb-2">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='/admin/user'">管理者ユーザ</buttton>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/cms.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

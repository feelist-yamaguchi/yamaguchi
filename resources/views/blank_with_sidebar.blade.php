@extends('layouts.default_with_sidebar')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('/js/app.js?now=@now()') }}"></script>
@endsection

@section('contents')
    <main>
        <div class="container">

            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-text-width"></i>
                            Description Horizontal
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-4">Description lists</dt>
                            <dd class="col-sm-8">A description list is perfect for defining terms.</dd>
                            <dt class="col-sm-4">Euismod</dt>
                            <dd class="col-sm-8">Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                            <dd class="col-sm-8 offset-sm-4">Donec id elit non mi porta gravida at eget metus.</dd>
                            <dt class="col-sm-4">Malesuada porta</dt>
                            <dd class="col-sm-8">Etiam porta sem malesuada magna mollis euismod.</dd>
                            <dt class="col-sm-4">Felis euismod semper eget lacinia</dt>
                            <dd class="col-sm-8">Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo
                                sit amet risus.
                            </dd>
                        </dl>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>

            <div class="row mt-3">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Quick Example</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </main>
@endsection

@section('sidebar_right')
    <div>
        <div class="card mt-3">
            <div class="card-header">
                <h4 class="card-title">
                    <i class="fas fa-text-width"></i>
                    Block Quotes
                </h4>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                </blockquote>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection

@include('layouts.head')
@include('layouts.header')
@include('layouts.sidebar_left')
@include('layouts.twitter_timeline')
@include('layouts.footer')
